package com.zxelec.sz.szanalyticadapter.domain.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Time {

    private List<Integer> days;
    private String start_time;
    private String end_time;
}
