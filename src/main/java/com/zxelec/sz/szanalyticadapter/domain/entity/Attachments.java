package com.zxelec.sz.szanalyticadapter.domain.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Attachments {

    private String id;
    private String name;
    private String mamId;
    private String minioPath;
    private String minioUrl;
}
