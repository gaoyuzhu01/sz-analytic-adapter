package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RepositoryRetrievalVO {

    private String faceImage;

    private String faceUrl;

    private String retrievalId;

    private RepositoryExtParamVO extParam;

}