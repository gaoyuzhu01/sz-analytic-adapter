package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NvnTaskVo {
    private String taskName;
    private Integer collisionType;
    private String oneRepositoryId;
    private String anotherRepositoryId;
    private Integer type;
    private Float threshold;
    private Integer topN;
}
