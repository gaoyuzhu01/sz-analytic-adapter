package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.Data;

/**
 * @author gaoyuzhu
 * @date 2023年09月21日 10:28
 */
@Data
public class Order {
    private String orderBy;
    private String order;
}
