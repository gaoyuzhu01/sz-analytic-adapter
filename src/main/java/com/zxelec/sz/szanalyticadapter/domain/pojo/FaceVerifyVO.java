package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FaceVerifyVO {

    private String srcFaceImage;
    private String dstFaceImage;
    private String vendor;
    private String deviceId;
}
