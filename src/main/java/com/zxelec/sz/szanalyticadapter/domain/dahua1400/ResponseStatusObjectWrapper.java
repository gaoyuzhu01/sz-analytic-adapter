package com.zxelec.sz.szanalyticadapter.domain.dahua1400;

import lombok.Data;

import java.util.List;

@Data
public class ResponseStatusObjectWrapper {

    private ResponseStatusList ResponseStatusListObject;


    @Data
    public class ResponseStatusList {
        private List<ResponseStatusObject> ResponseStatusObject;
    }

    @Data
    public class ResponseStatusObject {
        private String Id;

        private String LocalTime;

        private String RequestURL;

        private int StatusCode;

        private String StatusString;

    }
}
