package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * created by yuexiangyu on 2023/9/1914:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskVO {

    private String surveyId;

    private String surveyName;

    private String repositoryId;

    private Integer surveyCategory;

    private Double threshold;

    private String startTime;

    private String endTime;

    private TaskExtInfoVO extInfo;

    private List<String> searchWords;

    private String memo;

    private Integer isPublic;

    private List<String> shareUserCodes;

    private List<String> shareRoleCodes;

    private String tel;

    private String surveyLevel;

    private Integer enableSmsAlarm;

    private String surveyUserCode;

    private String surveyUserName;

    private String surveySource;

    private Integer siteChooseMethod;

    private List<String> chooseMethodCodes;

    private String reviewGroupCode;
}
