package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * created by yuexiangyu on 2023/9/1914:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TasksQueryReviewVO {
    private Integer page;
    private Integer pageSize;
    private List<String> surveySource;
    private Integer surveyType;
    private Integer surveyCategory;
    private List<Integer> surveyStatus;
    private List<Integer> reviewStatus;
    private Integer isPublic;
    private String keyword;
}
