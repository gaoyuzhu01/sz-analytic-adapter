package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author gaoyuzhu
 * @date 2023年09月19日 14:36
 */
@Data
public class QueryThemeDTO {
    private Integer page;
    private Integer pageSize;
    private List<String> repositoryType;
    private List<String> sourceType;
    private String repositoryGroupCode;
    private List<String> category;
    private Integer needReview;
    private Integer isShare;
    private Integer isPublic;
    private Integer repositoryTheme;
    private String keyword;
    private Integer repositorySurveyStatus;
    private Integer returnSurveyStatus;
    private List<String> repositoryIds;
    private String modifyTimeStart;
    private String modifyTimeEnd;
    private List<Integer> issuedTypes;
    private List<Order> order;


}
