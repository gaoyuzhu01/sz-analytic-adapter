package com.zxelec.sz.szanalyticadapter.domain.pojo;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RepositoryExtParamVO {
    private Integer leftEyeX;

    private Integer leftEyeY;

    private Integer rightEyeX;

    private Integer rightEyeY;

    private Integer noseX;

    private Integer noseY;

    private Integer leftMouthX;

    private Integer leftMouthY;

    private Integer rightMouthX;

    private Integer rightMouthY;

}