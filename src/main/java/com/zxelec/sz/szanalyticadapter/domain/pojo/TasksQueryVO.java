package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * created by yuexiangyu on 2023/10/1811:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TasksQueryVO {
    private Integer page;
    private Integer pageSize;
    private List<String> surveySource;
    private List<String> repositoryType;
    private List<String> repositoryIds;
    private List<Integer> surveyType;
    private String surveyCategory;
    private List<Integer> surveyStatus;
    private List<Integer> reviewStatus;
    private String isPublic;
    private String isShare;
    private String keyword;
    private List<Integer> surveyIds;
    private String modifyTimeStart;
    private String modifyTimeEnd;
    private String createTimeStart;
    private String createTimeEnd;
    private List<Integer> objTypes;
    private String orderBy;
    private String order;


}
