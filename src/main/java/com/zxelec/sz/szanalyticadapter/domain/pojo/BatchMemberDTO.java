package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author gaoyuzhu
 * @date 2023年09月19日 13:26
 */
@Data
public class BatchMemberDTO {
    private String repositoryId;
    private List<MemberDTO> memberList;



}
