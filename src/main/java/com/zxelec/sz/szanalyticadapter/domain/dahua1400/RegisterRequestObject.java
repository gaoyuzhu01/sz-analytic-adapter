package com.zxelec.sz.szanalyticadapter.domain.dahua1400;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RegisterRequestObject {
    private RegisterObject RegisterObject;
    @Data
    public static class RegisterObject {
        private String DeviceID;
    }
}
