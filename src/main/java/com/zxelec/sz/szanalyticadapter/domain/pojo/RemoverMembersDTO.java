package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author gaoyuzhu
 * @date 2023年09月19日 10:12
 */
@Data
public class RemoverMembersDTO {

    private String repositoryId;
    private String repositoryType;
    private List memberIds;
}
