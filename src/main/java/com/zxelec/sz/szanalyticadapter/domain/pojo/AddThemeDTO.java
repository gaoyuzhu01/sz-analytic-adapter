package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author gaoyuzhu
 * @date 2023年09月19日 15:04
 */
@Data
public class AddThemeDTO {
    private String repositoryId;
    private String sourceType;
    private String repositoryName;
    private String repositoryType;
    private String category;
    private String repositoryLevel;
    private Integer repositoryTheme;
    private String repositoryGroupCode;
    private List<String> tagCodes;
    private Integer isPublic;
    private List<String> shareUserCodes;
    private List<String> shareRoleCodes;
    private Integer needReview;
    private String reviewGroupCode;
    private ExtInfo extInfo;
    private List<String> searchWords;
    private String memo;
    private List<Integer> issuedTypes;
    private List<String> deviceCodes;
}
