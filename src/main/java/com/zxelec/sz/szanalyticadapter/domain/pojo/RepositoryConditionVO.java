package com.zxelec.sz.szanalyticadapter.domain.pojo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RepositoryConditionVO {
    private String queryId;

    private String identificationId;

    private String name;

    private List<String> repositoryIds;

    private List<RepositoryRetrievalVO> retrieval;

    private Double threshold;

}