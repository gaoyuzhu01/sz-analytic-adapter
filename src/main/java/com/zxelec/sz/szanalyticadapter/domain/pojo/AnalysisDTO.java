package com.zxelec.sz.szanalyticadapter.domain.pojo;

import com.zxelec.sz.szanalyticadapter.domain.entity.Domain_power;
import com.zxelec.sz.szanalyticadapter.domain.entity.Extend_info;
import com.zxelec.sz.szanalyticadapter.domain.entity.Source;
import com.zxelec.sz.szanalyticadapter.domain.entity.Time;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AnalysisDTO {

    private Integer source_type;

    private Integer priority_level;

    private Integer status;

    private String search;

    private String task_name;

    private List<Source> sources;

    private List<Domain_power> domain_power;

    private Extend_info extend_info;

    private String analysis_mode;

    private List<Time> times;

    private List<Integer> belong_people;

    private Integer is_public;

    private Integer aggregate;

    private Integer page;

    private Integer size;

    private Long task_create_time;

    private Long task_end_time;

    private Long video_start_time;

    private Long video_end_time;

}
