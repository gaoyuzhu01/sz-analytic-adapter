package com.zxelec.sz.szanalyticadapter.domain.dahua1400;

import lombok.Data;

@Data
public class KeepaliveRequestObject {

    private KeepaliveObject KeepaliveObject;
    @Data
    public static class KeepaliveObject {
        private String DeviceID;
    }
}
