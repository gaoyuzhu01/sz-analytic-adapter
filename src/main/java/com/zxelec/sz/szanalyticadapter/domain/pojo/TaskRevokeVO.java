package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * created by yuexiangyu on 2023/9/1914:49
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskRevokeVO {

    private List<String> surveyIds;
}
