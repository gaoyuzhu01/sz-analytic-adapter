package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CaptureRetrievalVO {
    private String ImageId;

    private String faceImage;

    private Double threshold;

    private String vendor;

}