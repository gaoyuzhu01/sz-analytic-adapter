package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * created by yuexiangyu on 2023/9/1914:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskExtInfoVO {

    private String color;
}
