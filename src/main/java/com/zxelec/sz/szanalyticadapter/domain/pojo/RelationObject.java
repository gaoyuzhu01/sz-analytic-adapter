package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.Data;

/**
 * @author gaoyuzhu
 * @date 2023年09月21日 14:12
 */
@Data
public class RelationObject {
    private String objType;
    private String objId;
}
