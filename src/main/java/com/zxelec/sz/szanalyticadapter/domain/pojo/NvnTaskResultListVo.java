package com.zxelec.sz.szanalyticadapter.domain.pojo;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NvnTaskResultListVo {
    private Integer page;
    private Integer pageSize;
    private String taskId;
    private String keyword;
    private Integer count;

}
