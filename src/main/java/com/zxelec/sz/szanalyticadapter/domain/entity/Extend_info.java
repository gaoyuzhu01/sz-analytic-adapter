package com.zxelec.sz.szanalyticadapter.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Extend_info {

    private Integer face;

    private Integer person;

    private Integer vehicle;

    private Integer nonvehicle;
}
