package com.zxelec.sz.szanalyticadapter.domain.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zxelec.sz.szanalyticadapter.enums.AlgorithmNameEnum;
import com.zxelec.sz.szanalyticadapter.enums.AlgorithmTypeEnum;
import com.zxelec.sz.szanalyticadapter.enums.ManufacturerNameEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MultipleAlgorithmsManagement {


    private String id;
    private String title;
    private Integer algorithmNameCode;
    private String algorithmName;
    private Integer algorithmTypeCode;
    private String algorithmType;
    private Integer manufacturerNameCode;
    private String manufacturerName;
    private String algorithmInterface;
    private String version;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp releaseTime;
    private String runningState;
    @TableField(exist = false)
    private Page page;


    public void setAlgorithmNameCode(Integer algorithmNameCode) {
        this.algorithmNameCode = algorithmNameCode;
        if (Objects.equals(algorithmNameCode, AlgorithmNameEnum.FACE_ALIGNMENT_ALGORITHMS.getCode())) {
            this.algorithmName = AlgorithmNameEnum.FACE_ALIGNMENT_ALGORITHMS.getMsg();
        }
        if (Objects.equals(algorithmNameCode, AlgorithmNameEnum.FACE_RECOGNITION_ANALYSIS_ALGORITHM.getCode())) {
            this.algorithmName = AlgorithmNameEnum.FACE_RECOGNITION_ANALYSIS_ALGORITHM.getMsg();
        }
        if (Objects.equals(algorithmNameCode, AlgorithmNameEnum.TARGET_IDENTIFICATION_ANALYSIS_ALGORITHM.getCode())) {
            this.algorithmName = AlgorithmNameEnum.TARGET_IDENTIFICATION_ANALYSIS_ALGORITHM.getMsg();
        }
        if (Objects.equals(algorithmNameCode, AlgorithmNameEnum.STRUCTURAL_ANALYSIS_ALGORITHM.getCode())) {
            this.algorithmName = AlgorithmNameEnum.STRUCTURAL_ANALYSIS_ALGORITHM.getMsg();
        }
        if (Objects.equals(algorithmNameCode, AlgorithmNameEnum.OBJECT_DETECTION.getCode())) {
            this.algorithmName = AlgorithmNameEnum.OBJECT_DETECTION.getMsg();
        }
        if (Objects.equals(algorithmNameCode, AlgorithmNameEnum.NON_MOTOR_VEHICLE_COMPARISON_ALGORITHM.getCode())) {
            this.algorithmName = AlgorithmNameEnum.NON_MOTOR_VEHICLE_COMPARISON_ALGORITHM.getMsg();
        }
        if (Objects.equals(algorithmNameCode, AlgorithmNameEnum.FACE_RECOGNITION.getCode())) {
            this.algorithmName = AlgorithmNameEnum.FACE_RECOGNITION.getMsg();
        }
        if (Objects.equals(algorithmNameCode, AlgorithmNameEnum.STRUCTURING.getCode())) {
            this.algorithmName = AlgorithmNameEnum.STRUCTURING.getMsg();
        }
        if (Objects.equals(algorithmNameCode, AlgorithmNameEnum.VEHICLE_COMPARISON_ALGORITHM.getCode())) {
            this.algorithmName = AlgorithmNameEnum.VEHICLE_COMPARISON_ALGORITHM.getMsg();
        }
        if (Objects.equals(algorithmNameCode, AlgorithmNameEnum.PERSONNEL_THAN_ALGORITHM.getCode())) {
            this.algorithmName = AlgorithmNameEnum.PERSONNEL_THAN_ALGORITHM.getMsg();
        }
        if (Objects.equals(algorithmNameCode, AlgorithmNameEnum.OBJECT_RECOGNITION.getCode())) {
            this.algorithmName = AlgorithmNameEnum.OBJECT_RECOGNITION.getMsg();
        }

    }

    public void setAlgorithmTypeCode(Integer algorithmTypeCode) {
        this.algorithmTypeCode = algorithmTypeCode;
        if (Objects.equals(algorithmTypeCode, AlgorithmTypeEnum.ANALYSIS_ALGORITHM.getCode())) {
            this.algorithmType = AlgorithmTypeEnum.ANALYSIS_ALGORITHM.getMsg();
        }
        if (Objects.equals(algorithmTypeCode, AlgorithmTypeEnum.ALIGNMENT_ALGORITHM.getCode())) {
            this.algorithmType = AlgorithmTypeEnum.ALIGNMENT_ALGORITHM.getMsg();
        }
        if (Objects.equals(algorithmTypeCode, AlgorithmTypeEnum.COMBINATORIAL_ALGORITHM.getCode())) {
            this.algorithmType = AlgorithmTypeEnum.COMBINATORIAL_ALGORITHM.getMsg();
        }
    }

    public void setManufacturerNameCode(Integer manufacturerNameCode) {
        this.manufacturerNameCode = manufacturerNameCode;
        if (Objects.equals(manufacturerNameCode, ManufacturerNameEnum.UNISINSIGHT.getCode())) {
            this.manufacturerName = ManufacturerNameEnum.UNISINSIGHT.getMsg();
        }
        if (Objects.equals(manufacturerNameCode, ManufacturerNameEnum.HAIKANG.getCode())) {
            this.manufacturerName = ManufacturerNameEnum.HAIKANG.getMsg();
        }
        if (Objects.equals(manufacturerNameCode, ManufacturerNameEnum.DAHUA.getCode())) {
            this.manufacturerName = ManufacturerNameEnum.DAHUA.getMsg();
        }


    }
}
