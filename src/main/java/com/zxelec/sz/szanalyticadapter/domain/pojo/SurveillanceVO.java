package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * created by yuexiangyu on 2023/9/2709:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SurveillanceVO {

    private Integer surveyType;

    private String plateNum;

    private String plateType;

    private String startTimeStr;

    private String endTimeStr;

    private String startTimeStrUtc;

    private String endTimeStrUtc;

    private String reason;

    private List<String> tels;

    private String surveyCategory;

    private String surveyTaskName;

    private List<String> libRecordIds;

    private List<String> channelCodes;

    private String paperSign;

    private Integer paperCnt;

    private String dropSign;

    private Integer dropCnt;

    private String tagSign;

    private Integer tagCnt;

    private String sunSign;

    private Integer sunCnt;


}
