package com.zxelec.sz.szanalyticadapter.domain.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PageResult {

    private List<?> data;
    private Long pageNum;
    private Long pageSize;
    private Long pages;
    private Long total;


    public PageResult(com.baomidou.mybatisplus.extension.plugins.pagination.Page<?> data) {
        this.data = data.getRecords();
        this.pageNum = data.getCurrent();
        this.pageSize = data.getSize();
        this.pages = data.getPages();

        this.total = data.getTotal();
    }
}
