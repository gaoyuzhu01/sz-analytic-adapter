package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author gaoyuzhu
 * @date 2023年09月19日 10:29
 */
@Data
public class MemberDTO {
    private String repositoryId;
    private String memberId;
    private String imsi;
    private String responsibilityUserCode;
    private String responsibilityUserName;
    private String tel;
    private String memo;
    private String repositoryType;
    private ExtInfo extInfo;
    private List<String> searchWords;
    private String rsmember;
    private String memberName;
    private String imgData;
    private String idType;
    private String idNumber;
    private String validityStartDate;
    private String validityEndDate;
    private String birthDay;
    private String englishName;
    private String memberTel;
    private String permanentAddr;
    private String dict;
    private String gender;
    private String nationalityCode;
    private String province;
    private String city;
    private String district;
    private List<RelationObject> relationObjects;
    private String objType;
    private String objId;
    private String plateNum;
    private String plateType;
    private String phone;
    private String macAddr;
    private String rfid;
}
