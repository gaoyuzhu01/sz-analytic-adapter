package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CaptureVO {

    private Integer start;

    private Integer limit;
    private Integer page;

    private Integer pageSize;

    private Integer count;

    private CaptureConditionVO condition;

    private List<CaptureRetrievalVO> retrieval;

    private List<CaptureOrderVO> order;


}