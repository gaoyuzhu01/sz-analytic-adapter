package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * created by yuexiangyu on 2023/10/1814:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RasdicbatchqueryVO {

    private List<String> typeCodes;
    private Integer stat;
}
