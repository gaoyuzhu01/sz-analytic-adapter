package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.Data;

/**
 * @author gaoyuzhu
 * @date 2023年09月19日 10:42
 */
@Data
public class ExtInfo {
    private String color;
}
