package com.zxelec.sz.szanalyticadapter.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResultToken {
    private String access_token;
    private Integer expires_in;
    private String refresh_token;
}
