package com.zxelec.sz.szanalyticadapter.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Source {

    private String source_id;

    private String source_name;

    private String type;

    private String sub_type;

    private String cascaded_id;
}
