package com.zxelec.sz.szanalyticadapter.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Domain_power {

    private String cascaded_id;
    private String cascaded_name;
    private Integer speed;
}
