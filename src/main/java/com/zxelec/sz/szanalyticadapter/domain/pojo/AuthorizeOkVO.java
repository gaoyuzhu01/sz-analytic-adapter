package com.zxelec.sz.szanalyticadapter.domain.pojo;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthorizeOkVO {
    private Integer duration;
    private String token;
    private String userName;
    private String userId;
    private String lastLoginIp;
    private String version;
}
