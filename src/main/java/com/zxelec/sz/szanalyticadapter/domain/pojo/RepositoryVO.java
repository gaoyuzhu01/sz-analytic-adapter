package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RepositoryVO {

    private Integer page;

    private Integer resultPictureType;

    private Integer pageSize;

    private RepositoryConditionVO condition;

    private List<RepositoryOrderVO> order;

}
