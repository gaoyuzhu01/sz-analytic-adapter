package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author gaoyuzhu
 * @date 2023年09月19日 13:47
 */
@Data
public class ThemeDTO {
    private String repositoryId;
    private String repositoryName;
    private String sourceType;
    private String repositoryType;
    private String category;
    private String repositoryLevel;
    private Integer repositoryTheme;
    private String repositoryGroupCode;
    private String repositoryGroupName;
    private List<Object> tags;
    private String tagCode;
    private String tagName;
    private String needReview;
    private String reviewGroupCode;
    private String reviewGroupName;
    private String extInfo;
    private List<String> searchWords;
    private Integer isPublic;
    private Integer isShare;
    private String memo;
    private String createTime;
    private String createUserCode;
    private String createUserName;
    private String modifyTime;
    private String modifyUserCode;
    private String modifyUserName;
    private List<String> issuedTypes;
    private List<String> deviceCodes;
}
