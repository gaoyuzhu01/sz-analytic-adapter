package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author gaoyuzhu
 * @date 2023年09月19日 14:30
 */
@Data
public class EditThemeVO {
    private String repositoryName;
    private String repositoryGroupCode;
    private List<String> tagCodes;
    private Integer isPublic;
    private List<String> shareUserCodes;
    private List<String> shareRoleCodes;
    private Integer needReview;
    private String reviewGroupCode;
    private String repositoryLevel;
    private ExtInfo extInfo;
    private List<String> searchWords;
    private String memo;
    private List<Integer> issuedTypes;
    private List<String> deviceCodes;
}
