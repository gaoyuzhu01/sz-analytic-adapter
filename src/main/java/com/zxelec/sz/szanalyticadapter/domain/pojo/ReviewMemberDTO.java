package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author gaoyuzhu
 * @date 2023年09月19日 13:32
 */
@Data
public class ReviewMemberDTO {
    private Integer reviewResult;
    private String repositoryId;
    private String repositoryType;
    private List<String> memberIds;
    private String reason;
}
