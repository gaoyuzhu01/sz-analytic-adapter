package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author gaoyuzhu
 * @date 2023年09月19日 13:35
 */
@Data
public class QueryReviewMemberDTO {
    private Integer page;
    private Integer pageSize;
    private List<String> sourceType;
    private String repositoryType;
    private String repositoryId;
    private List<Integer> memberStatus;
    private String keyword;
}
