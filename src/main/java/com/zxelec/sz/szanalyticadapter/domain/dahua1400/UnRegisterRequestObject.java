package com.zxelec.sz.szanalyticadapter.domain.dahua1400;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UnRegisterRequestObject {
    private UnRegisterObject UnRegisterObject;

    @Data
    public static class UnRegisterObject {
        private String DeviceID;
    }
}
