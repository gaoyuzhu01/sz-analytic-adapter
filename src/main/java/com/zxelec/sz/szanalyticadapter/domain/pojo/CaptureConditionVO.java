package com.zxelec.sz.szanalyticadapter.domain.pojo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CaptureConditionVO {

    private List<String> orgCodes;

    private List<String> channelCodes;

    private String startCapTime;

    private String endCapTime;

    private Integer startAge;

    private Integer endAge;

    private Integer gender;

    private Integer fringe;

    private Integer eye;

    private Integer mouth;

    private Integer beard;

    private Integer mask;

    private Integer glasses;

    private Integer emotion;

    private String extractedFlag;
    
    private String plateNum;

}