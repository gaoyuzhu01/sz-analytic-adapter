package com.zxelec.sz.szanalyticadapter.domain.pojo;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthorizeUnauthorizedVO {
    private String realm;
    private String randomKey;
    private String encryptType;
    private String method;
}
