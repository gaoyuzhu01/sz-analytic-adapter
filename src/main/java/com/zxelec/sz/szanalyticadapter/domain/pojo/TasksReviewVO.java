package com.zxelec.sz.szanalyticadapter.domain.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * created by yuexiangyu on 2023/9/2110:45
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TasksReviewVO {

    private Integer reviewResult;
    private List<String> surveyIds;
    private String reason;
}
