package com.zxelec.sz.szanalyticadapter.controller;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.zxelec.sz.szanalyticadapter.domain.entity.Result;
import com.zxelec.sz.szanalyticadapter.domain.pojo.*;
import com.zxelec.sz.szanalyticadapter.service.DaHuaService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@RestController
@Slf4j
@RequestMapping("daHua")
public class DaHuaController {

    @Autowired
    private DaHuaService daHuaService;


    @GetMapping("/detect")
    public Result detect() {
        return daHuaService.detect();
    }

    @PostMapping("/rasdicbatchquery")
    public Result rasdicbatchquery(@RequestBody RasdicbatchqueryVO vo) {
        return daHuaService.rasdicbatchquery(vo);
    }

    @PostMapping("/capture")
    public Result capture(@RequestBody CaptureVO vo) {

        return daHuaService.capture(vo);
    }
    @PostMapping("/surveillance")
    public Result surveillance(@RequestBody SurveillanceVO vo) {
        return daHuaService.surveillance(vo);
    }


    @PostMapping("/repository")
    public Result repository(@RequestBody RepositoryVO vo) {

        return daHuaService.repository(vo);
    }

    @PostMapping("/nvnTask")
    public Result nvnTask(@RequestBody NvnTaskVo vo) {
        return daHuaService.nvnTask(vo);
    }

    @PostMapping("/faceVerify")
    public Result faceVerify(@RequestBody FaceVerifyVO vo) {
        return daHuaService.faceVerify(vo);
    }

    @PostMapping("/nvnTaskResultList")
    public Result nvnTaskResultList(@RequestBody NvnTaskResultListVo vo) {
        return daHuaService.nvnTaskResultList(vo);
    }

    @PostMapping("/task")
    public Result task(@RequestBody TaskVO vo) {
        return daHuaService.task(vo);
    }

    @PostMapping("/taskReview")
    public Result taskReview(@RequestBody TasksReviewVO vo) {
        return daHuaService.tasksReview(vo);
    }


    @PostMapping("/tasksQuery")
    public Result tasksQuery(@RequestBody TasksQueryVO vo) {
        return daHuaService.tasksQuery(vo);
    }

    @PostMapping("/tasksQueryReview")
    public Result tasksQueryReview(@RequestBody TasksQueryReviewVO vo) {
        return daHuaService.tasksQueryReview(vo);
    }

    @PostMapping("/taskUpdate")
    public Result taskUpdate(@RequestBody TaskVO vo) {
        return daHuaService.taskUpdate(vo);
    }

    @PostMapping("/taskRevoke")
    public Result taskRevoke(@RequestBody TaskRevokeVO vo) {
        return daHuaService.taskRevoke(vo);
    }


    @PostMapping("removeMembers")
    public Result removeMembers(@RequestBody RemoverMembersDTO t) {
        return daHuaService.removeMembers(t);
    }

    @PostMapping("addMember")
    public Result addMember(@RequestBody MemberDTO t) {
        return daHuaService.addMember(t);
    }

    @PostMapping("addBatchMember")
    public Result addBatchMember(@RequestBody BatchMemberDTO t) {
        return daHuaService.addBatchMember(t);
    }

    @PostMapping("reviewMember")
    public Result reviewMember(@RequestBody ReviewMemberDTO t) {
        return daHuaService.reviewMember(t);
    }
    @PostMapping("queryReviewMember")
    public Result queryReviewMember(@RequestBody QueryReviewMemberDTO t) {
        return daHuaService.queryReviewMember(t);
    }

    /**
     *  视图库详情
     * @param repositoryId
     * @return
     */
    @GetMapping("themeRepository")
    public Result themeRepository(@RequestParam String repositoryId) {
        return daHuaService.themeRepository(repositoryId);
    }

    @DeleteMapping("deletethemeRepository")
    public Result deletethemeRepository(@RequestParam String repositoryId) {
        return daHuaService.deletethemeRepository(repositoryId);
    }

    @PutMapping("editThemeRepository")
    public Result editThemeRepository(@RequestParam String repositoryId, @RequestBody EditThemeVO t) {
        return daHuaService.editThemeRepository(repositoryId,t);
    }

    @PostMapping("dahuaVeQuery")
    public Result dahuaVeQuery( @RequestBody JSONObject json) {
        return daHuaService.picQuery(json);
    }

    @PostMapping("queryThemeRepository")
    public Result queryThemeRepository( @RequestBody QueryThemeDTO t) {
        return daHuaService.queryThemeRepository(t);
    }

    @PostMapping("addThemeRepository")
    public Result addThemeRepository( @RequestBody AddThemeDTO t) {
        return daHuaService.addThemeRepository(t);
    }
    public static void main(String[] args) {
        File file = new File("/Users/yuexiangyu/Downloads/0c36011923d7d908880d85c4a8f57798.jpg");
        File newImage = new File("/Users/yuexiangyu/Downloads/23432.jpg");
        byte[] bytes;
        try {
            bytes = FileUtils.readFileToByteArray(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String base64 = new String(Base64.getEncoder().encode(bytes), StandardCharsets.UTF_8);
        String base641 = "KAQAAAEAAAACAAAAAAABAAAAAAAAAAAAAAAAAAAAAADAG2CCAAQAANnH2DziDKA9xwRuvGQ707yRLcc8qN+NvUNLIT4iYmo9n7qivTxYF71sbpW8YOXnumxulTxmJgE+pltvveOFNLsncaK9FM6TvSU/wTyUkYk9HsXLu2kqLz1w/Vy9t8WhvHTaMz0ajzy9E5wyvXnJD76T2Lw8mZnlvO/uBb0SIx69YZ40vJ2WGL0m2LE9f+NwPa1O+Tui/gi8vsaCOzRFMTxJk7W5g8DHvTRFMb1i0JW9MYi2vSHpVT0suTY9JrjVPcpoML0pHJi9R6GMPSzZEj3tvCQ8xADAPFYODLyddrw9vJQhvQGSFD3vzik98AALPTzRKz1cj3y9fb9mPHKo0r0FiOs8DTTCvQupqLw4e8A9TtvJPXq7uL3XXJu9epvcPZLGt7yZYIm8vWbuO1H/Uz1qwx+9BU+PuxHxPD7bcs685Je5vFzPtL3V+Ng9DALhvUksJjsX8p09fvHHPI33tzwrh1W7pruDPDbQSj2ovzG9ZbTnO+HaPr3LASE9gY5mPWd/Ob3vrk07Y2kGPbX6Tz028Ca9DCK9uz7jMDw4m5w6mdmdvRiLDj5NqWi9Ax2uvWv1gD30vYU9I9QDvtONmz3xeR8+byuQvVsWaLsT9Wo8vHTFvY6whD0kTZg92geRvTgiiDzOvhu91H9Evf0bTb3kHiW8/pRhvOCBhjwPZqO8Z19dPQ9mI72l4lq9t8UhPqoq7z1bFui7+3DXPDn0VD3Ovhu9AXK4vRHR4L2Yp7y8WLmBPCXGLL11DBU9+7CPvbfseL3rEa+9GEtWPfwppL2muwO9Itv+PRYZ9T2H1no9uF4SPrU6iL2of3m93CubPeit7D1dwd08JC28vY6whLvXo868YrC5vH/jcL1q/Pu5ZHsLvZ0PrT1dwd29NEWxO70tkr0DHS69U8qlPVT8hrx938I8rgdGPSvHDb2lIpM9xVn4PAJkYTwQeCg9BJbCO/F5n712/j07NneSvc5+YzzcKxu99L2FPQBA17tu2VI8LTLLvGFefLwgF4k8DAJhvAe6zDzRAgK8CzCUPGAloDxkO9O8gRVSO0/N8ro7n0o9v5hPPdECAr7PV4y9gVWKPGX0n706bWk9xIcrPOkNAb6BrsK9prsDvZWjDjuW/EY9XpqGumkKUz0LMJS9asOfPGxOOb0YS9Y84fqaPQILKb3tfGy8dczcPRqvmDlJDMo75B4lvTxYFz7tA1i8N0lfPYXk0bqCB3u9P3yhPaNXQT237Pi9SSwmvM2MOr1Ftt6818OqPIJnDzy7ghw9G0gJPHDEgD0e5ac8NKzAPZ2WGDynrSy9irPRPbu7eL1sLl299c+KPesxizoffpg9DVQevXhp+719v2Y9PgMNPdluID4=";
        System.out.println(base64);
        // Base64 保存为图片
        bytes = Base64.getDecoder().decode(base641);
        try {
            FileUtils.writeByteArrayToFile(newImage, bytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
