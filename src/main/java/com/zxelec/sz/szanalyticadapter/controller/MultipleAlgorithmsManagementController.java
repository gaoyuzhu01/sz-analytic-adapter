package com.zxelec.sz.szanalyticadapter.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxelec.sz.szanalyticadapter.domain.entity.MultipleAlgorithmsManagement;
import com.zxelec.sz.szanalyticadapter.domain.entity.PageResult;
import com.zxelec.sz.szanalyticadapter.domain.entity.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.zxelec.sz.szanalyticadapter.service.IMultipleAlgorithmsManagementService;

import java.sql.Timestamp;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/multipleAlgorithmsManagement")
public class MultipleAlgorithmsManagementController {

    @Autowired
    private IMultipleAlgorithmsManagementService multipleAlgorithmsManagementService;

    @PostMapping("/add")
    public Result add(@RequestBody MultipleAlgorithmsManagement data) {
        data.setId(IdWorker.getIdStr());
        data.setReleaseTime(new Timestamp(System.currentTimeMillis()));
        return Result.success(multipleAlgorithmsManagementService.save(data));
    }


    @PostMapping("/search")
    public Result search(@RequestBody MultipleAlgorithmsManagement data) {
        return multipleAlgorithmsManagementService.search(data);

    }


    @PostMapping("/update")
    public Result update(@RequestBody MultipleAlgorithmsManagement data) {
        return Result.success(multipleAlgorithmsManagementService.updateById(data));
    }


    @PostMapping("/delete")
    public Result delete(@RequestBody List<String> ids) {
        return Result.success(multipleAlgorithmsManagementService.removeByIds(ids));
    }


}
