package com.zxelec.sz.szanalyticadapter.controller;

import com.zxelec.sz.szanalyticadapter.domain.dahua1400.FaceInfoReq;
import com.zxelec.sz.szanalyticadapter.domain.entity.Result;
import com.zxelec.sz.szanalyticadapter.service.DaHua1400Service;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * created by yuexiangyu on 2023/11/1415:42
 */
@RestController
@Slf4j
@RequestMapping("daHua1400")
public class DaHua1400Controller {

    @Autowired
    private DaHua1400Service daHua1400Service;

    @PostMapping("/faces")
    public Result faces(@RequestBody FaceInfoReq faceInfoReq){
        return daHua1400Service.faces(faceInfoReq);
    }

    @GetMapping("/faces/{faceId}")
    public Result faces(@PathVariable("faceId") String faceId){
        return daHua1400Service.faces(faceId);
    }


    @GetMapping("/motorVehicles/{motorVehicleId}")
    public Result motorVehicles(@PathVariable("motorVehicleId") String motorVehicleId){
        return daHua1400Service.motorVehicles(motorVehicleId);
    }
}
