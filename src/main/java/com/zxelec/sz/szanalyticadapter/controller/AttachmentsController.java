package com.zxelec.sz.szanalyticadapter.controller;

import com.zxelec.sz.szanalyticadapter.domain.entity.Attachments;
import com.zxelec.sz.szanalyticadapter.domain.entity.Result;
import com.zxelec.sz.szanalyticadapter.service.IAttachmentsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@Slf4j
@RequestMapping("/attachments")
public class AttachmentsController {

    @Autowired
    private IAttachmentsService attachmentsService;


    @PostMapping("/upload")
    public Result upload(@RequestParam("file") MultipartFile[] files, String mamId) {
        if (files.length == 0) {
            return Result.error("最少上传一个文件");
        }
        StringBuffer stringBuffer = new StringBuffer();
        try {
            for (MultipartFile file : files) {
                Result ajaxResult = attachmentsService.upload(file.getInputStream(), file.getOriginalFilename(), file.getContentType(), mamId);
                stringBuffer.append(file.getOriginalFilename() + "---" + ajaxResult.get(Result.MSG_TAG));
                stringBuffer.append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Result.success(stringBuffer);
    }


    @PostMapping("/delete")
    public Result delete(@RequestBody Attachments attachments) {
        return attachmentsService.delete(attachments);

    }

    @PostMapping("/list")
    public Result list(@RequestBody Attachments attachments) {

        return attachmentsService.list(attachments.getMamId());
    }

}
