package com.zxelec.sz.szanalyticadapter.controller;

import com.zxelec.sz.szanalyticadapter.service.UnisinsightService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class UnisinsightController {

    @Autowired
    private UnisinsightService unisinsightService;


    @GetMapping("test")
    public void test() {

        Object o = unisinsightService.getAnalysisTaskAnalysisDetailForTaskId();
        System.out.println(o);
    }
}
