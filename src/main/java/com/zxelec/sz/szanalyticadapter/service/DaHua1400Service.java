package com.zxelec.sz.szanalyticadapter.service;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSON;
import com.zxelec.sz.szanalyticadapter.domain.dahua1400.*;
import com.zxelec.sz.szanalyticadapter.domain.entity.Result;
import com.zxelec.sz.szanalyticadapter.task.DelayQueueManager;
import com.zxelec.sz.szanalyticadapter.task.DelayTask;
import com.zxelec.sz.szanalyticadapter.task.Prefix;
import com.zxelec.sz.szanalyticadapter.util.DigestUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * created by yuexiangyu on 2023/11/1309:25
 */
@Service
@Slf4j
public class DaHua1400Service {

    @Resource(name = "daHua14000RestTemplate")
    private RestTemplate restTemplate;

    private static final String REGISTER = "/VIID/System/Register";
    private static final String UNREGISTER = "/VIID/System/UnRegister";
    private static final String KEEPALIVE = "/VIID/System/Keepalive";
    private static final String FACES = "/VIID/Faces";
    private static final String MOTORVEHICLES = "/VIID/MotorVehicles";

    public static DelayQueueManager delayQueueManager;

    @Value("${sz.dahua1400.deviceId}")
    private String deviceId;
    @Value(value = "${sz.dahua1400.username}")
    private String username;

    @Value(value = "${sz.dahua1400.password}")
    private String password;

    @Value(value = "${sz.dahua1400.enable}")
    private Boolean enable;

    @PostConstruct
    public void init(){
        if (enable){
            register();
        }
    }

    private void register() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/json; charset=UTF-8"));
        headers.set("User-Identify", deviceId);
        headers.setConnection("keepalive");
        // 请求参数设置
        RegisterRequestObject registerRequestObject = new RegisterRequestObject();
        RegisterRequestObject.RegisterObject registerObject = new RegisterRequestObject.RegisterObject();
        registerObject.setDeviceID(deviceId);
        registerRequestObject.setRegisterObject(registerObject);

        HttpEntity<String> httpEntity = new HttpEntity<>(JSONUtil.toJsonStr(registerRequestObject), headers);
        // 第一次请求
        ResponseEntity<String> responseEntity = restTemplate.exchange(REGISTER, HttpMethod.POST, httpEntity, String.class);
        if (org.apache.http.HttpStatus.SC_UNAUTHORIZED == responseEntity.getStatusCode().value()) {
            HttpHeaders responseEntityHeaders = responseEntity.getHeaders();
            String authenticate = responseEntityHeaders.get("WWW-Authenticate").get(0);
            String[] children = authenticate.split(",");
            // Digest realm="myrealm",qop="auth",nonce="dmktZGlnZXN0OjQzNTQyNzI3Nzg="
            String realm = null, qop = null, nonce = null, opaque = null, method = "POST";
            ;
            for (int i = 0; i < children.length; i++) {
                String item = children[i];
                String[] itemEntry = item.split("=");
                if (itemEntry[0].equals("Digest realm")) {
                    realm = itemEntry[1].replaceAll("\"", "");
                } else if (itemEntry[0].equals("qop")) {
                    qop = itemEntry[1].replaceAll("\"", "");
                } else if (itemEntry[0].equals("nonce")) {
                    nonce = itemEntry[1].replaceAll("\"", "");
                }
            }
            String nc = "00000001";
            String cnonce = DigestUtils.generateSalt2(8);
//            String response = DigestUtils.getResponse(username, realm, password, nonce, nc, cnonce, qop, method, REGISTER);
            String response = DigestUtils.getResponse(null, realm, null, nonce, nc, cnonce, qop, method, REGISTER);
//            String authorization = DigestUtils.getAuthorization(username, realm, nonce, REGISTER, qop, nc, cnonce, response, opaque);
            String authorization = DigestUtils.getAuthorization(null, realm, nonce, REGISTER, qop, nc, cnonce, response, opaque);
            headers.set("Authorization", authorization);

            // 第二次请求
            httpEntity = new HttpEntity<>(JSONUtil.toJsonStr(registerRequestObject), headers);
            responseEntity = restTemplate.exchange(REGISTER, HttpMethod.POST, httpEntity, String.class);
            if (org.apache.http.HttpStatus.SC_OK == responseEntity.getStatusCode().value()) {
                log.info("大华1400注册成功");
                delayQueueManager = new DelayQueueManager();
                delayQueueManager.put(new DelayTask(Prefix.keepalive, deviceId, 1000 * 60, this::keepalive));
            } else {
                log.error("大华1400注册失败");
            }
        } else {
            log.error("大华1400注册失败");
        }
    }

    private void keepalive() {
// 请求头设置
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/json; charset=UTF-8"));
        headers.set("User-Identify", deviceId);
        headers.setConnection("keepalive");
        // 请求参数设置
        KeepaliveRequestObject keepaliveRequestObject = new KeepaliveRequestObject();
        KeepaliveRequestObject.KeepaliveObject keepaliveObject = new KeepaliveRequestObject.KeepaliveObject();
        keepaliveObject.setDeviceID(deviceId);
        keepaliveRequestObject.setKeepaliveObject(keepaliveObject);
        log.info("保活请求 url:{} ,参数：{}", KEEPALIVE, keepaliveRequestObject);

        HttpEntity<String> httpEntity = new HttpEntity<>(JSONUtil.toJsonStr(keepaliveRequestObject), headers);
        // 请求执行
        ResponseEntity<String> responseEntity = restTemplate.exchange(KEEPALIVE, HttpMethod.POST, httpEntity, String.class);
        if (org.apache.http.HttpStatus.SC_OK == responseEntity.getStatusCode().value()) {
            log.info("大华1400保活成功");
            delayQueueManager.put(new DelayTask(Prefix.keepalive, deviceId, 1000 * 60, this::keepalive));
        } else {
            log.info("大华1400保活失败");
            register();
        }
    }

    @PreDestroy
    public void  destroy() {
        if (enable) {
            unRegister();
        }
    }

    private void unRegister() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/json; charset=UTF-8"));
        headers.set("User-Identify", deviceId);
        headers.setConnection("keepalive");
        // 请求参数设置
        UnRegisterRequestObject unRegisterRequestObject = new UnRegisterRequestObject();
        UnRegisterRequestObject.UnRegisterObject unRegisterObject = new UnRegisterRequestObject.UnRegisterObject();
        unRegisterObject.setDeviceID(deviceId);
        unRegisterRequestObject.setUnRegisterObject(unRegisterObject);

        HttpEntity<String> httpEntity = new HttpEntity<>(JSONUtil.toJsonStr(unRegisterRequestObject), headers);
        // 第一次请求
        ResponseEntity<String> responseEntity = restTemplate.exchange(UNREGISTER, HttpMethod.POST, httpEntity, String.class);
        if (org.apache.http.HttpStatus.SC_UNAUTHORIZED == responseEntity.getStatusCode().value()) {
            HttpHeaders responseEntityHeaders = responseEntity.getHeaders();
            String authenticate = responseEntityHeaders.get("WWW-Authenticate").get(0);
            String[] children = authenticate.split(",");
            // Digest realm="myrealm",qop="auth",nonce="dmktZGlnZXN0OjQzNTQyNzI3Nzg="
            String realm = null, qop = null, nonce = null, opaque = null, method = "POST";
            ;
            for (int i = 0; i < children.length; i++) {
                String item = children[i];
                String[] itemEntry = item.split("=");
                if (itemEntry[0].equals("Digest realm")) {
                    realm = itemEntry[1].replaceAll("\"", "");
                } else if (itemEntry[0].equals("qop")) {
                    qop = itemEntry[1].replaceAll("\"", "");
                } else if (itemEntry[0].equals("nonce")) {
                    nonce = itemEntry[1].replaceAll("\"", "");
                }
            }
            String nc = "00000001";
            String cnonce = DigestUtils.generateSalt2(8);
            String response = DigestUtils.getResponse(null, realm, null, nonce, nc, cnonce, qop, method, UNREGISTER);
//            String response = DigestUtils.getResponse(username, realm, password, nonce, nc, cnonce, qop, method, UNREGISTER);
            String authorization = DigestUtils.getAuthorization(null, realm, nonce, UNREGISTER, qop, nc, cnonce, response, opaque);
//            String authorization = DigestUtils.getAuthorization(username, realm, nonce, UNREGISTER, qop, nc, cnonce, response, opaque);
            headers.set("Authorization", authorization);

            // 第二次请求
            httpEntity = new HttpEntity<>(JSONUtil.toJsonStr(unRegisterRequestObject), headers);
            responseEntity = restTemplate.exchange(UNREGISTER, HttpMethod.POST, httpEntity, String.class);
            if (org.apache.http.HttpStatus.SC_OK == responseEntity.getStatusCode().value()) {
                log.info("大华1400注销成功");
            } else {
                log.error("大华1400注销失败");
            }
        } else {
            log.error("大华1400注销失败");
        }

    }

    public Result faces(FaceInfoReq faceInfoReq) {
        String time = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").format(LocalDateTime.now());
        FaceRequestObject faceRequestObject = new FaceRequestObject();
        FaceRequestObject.FaceListObject faceListObject = new FaceRequestObject.FaceListObject();
        faceRequestObject.setFaceListObject(faceListObject);

        List<FaceRequestObject.Face> faceList = new ArrayList<>();
        FaceRequestObject.Face face = new FaceRequestObject.Face();
        face.setInfoKind(0); // 信息分类 0:其他 1:自动采集 2:人工采集
        face.setLeftTopX(faceInfoReq.getLeftTopX());
        face.setLeftTopY(faceInfoReq.getLeftTopY());
        face.setRightBtmX(faceInfoReq.getRightBtmX());
        face.setRightBtmY(faceInfoReq.getRightBtmY());
        face.setIsForeigner(0); // 是否涉外人员
        face.setIsSuspectedTerrorist(0);// 是否涉恐人员
        face.setIsCriminalInvolved(0); // 是否涉案人员
        face.setIsDetainees(0); // 是否在押人员
        face.setIsVictim(0); //是否被害人
        face.setIsSuspiciousPerson(0); // 是否可疑人

        // 图像信息基本要素ID
        String sourceId = faceInfoReq.getDeviceId() + "02" + time + "00001";
        face.setSourceID(sourceId);  // 图像基本要素ID String(41)

        // 图像信息内容要素ID
        String faceId = sourceId + "06" + "00001";// 子类型编码 06-人脸
        face.setFaceID(faceId); // 人脸ID String(48)

        face.setDeviceID(faceInfoReq.getDeviceId());// 设备ID

        // 图片子对象信息
        FaceRequestObject.SubImageList subImageList = new FaceRequestObject.SubImageList();
        SubImageInfo subImageInfo = new SubImageInfo();
        subImageInfo.setDeviceID(faceInfoReq.getDeviceId());
        subImageInfo.setData(faceInfoReq.getImage());
        subImageInfo.setImageID(sourceId);
        subImageInfo.setEventSort(2); //事件类型 过人:2
        subImageInfo.setShotTime(time);
        subImageInfo.setFileFormat("Jpeg");
        subImageInfo.setType("11"); // 图像类型 人脸图：11
        subImageInfo.setWidth(faceInfoReq.getWidth());
        subImageInfo.setHeight(faceInfoReq.getHeight());

        List<SubImageInfo> subImageInfoList = new ArrayList<>();
        subImageInfoList.add(subImageInfo);
        subImageList.setSubImageInfoObject(subImageInfoList);
        face.setSubImageList(subImageList);
        faceList.add(face);
        faceListObject.setFaceObject(faceList);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/json;charset=utf-8"));
        headers.set("User-Identify", faceInfoReq.getDeviceId());
        HttpEntity<String> httpEntity = new HttpEntity<>(JSONUtil.toJsonStr(faceRequestObject), headers);
        log.info("上传人脸消息体：{}", JSONUtil.toJsonStr(faceRequestObject));

        ResponseEntity<String> responseEntity = restTemplate.exchange(FACES, HttpMethod.POST, httpEntity, String.class);
        log.info("上传人脸响应结果：{}", responseEntity);
        int statusCode = responseEntity.getStatusCode().value();
        if (statusCode == org.apache.http.HttpStatus.SC_OK) {
            String responseBody = responseEntity.getBody();
            if (StrUtil.isNotBlank(responseBody)) {
                ResponseStatusObjectWrapper responseStatusObjectWrapper = JSONUtil.toBean(responseBody, ResponseStatusObjectWrapper.class);
                int uploadStatus = responseStatusObjectWrapper.getResponseStatusListObject().getResponseStatusObject().get(0).getStatusCode();
                if (uploadStatus == 0) {
                    return Result.success(responseStatusObjectWrapper);
                }
            }
        }
        return Result.error();
    }

    public Result faces(String faceId){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/json;charset=utf-8"));
        headers.set("User-Identify", deviceId);
        HttpEntity<String> httpEntity = new HttpEntity<>(JSONUtil.toJsonStr(""), headers);

        ResponseEntity<String> responseEntity = restTemplate.exchange(FACES+"/"+faceId, HttpMethod.GET, httpEntity, String.class);
        log.info("查询人脸响应结果：{}", responseEntity);
        return  Result.success(responseEntity);
    }

    public Result motorVehicles(String motorVehicleId){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/json;charset=utf-8"));
        headers.set("User-Identify", deviceId);
        HttpEntity<String> httpEntity = new HttpEntity<>(JSONUtil.toJsonStr(""), headers);

        ResponseEntity<String> responseEntity = restTemplate.exchange(MOTORVEHICLES+"/"+motorVehicleId, HttpMethod.GET, httpEntity, String.class);
        log.info("查询人脸响应结果：{}", responseEntity);
        return  Result.success(responseEntity);
    }
}
// 130304211911902010610220231114163059000010600001
