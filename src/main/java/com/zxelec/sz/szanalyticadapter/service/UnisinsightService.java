package com.zxelec.sz.szanalyticadapter.service;

import com.alibaba.fastjson2.JSON;
import com.zxelec.sz.szanalyticadapter.domain.entity.ResultToken;
import com.zxelec.sz.szanalyticadapter.domain.entity.Source;
import com.zxelec.sz.szanalyticadapter.domain.pojo.AnalysisDTO;
import com.zxelec.sz.szanalyticadapter.util.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
@EnableScheduling
public class UnisinsightService {

    @Resource(name = "unisinsightRestTemplate")
    private RestTemplate restTemplate;

    private static final String ACCESS_TOKEN_URL = "/sso/oauth2.0/accessToken";
    private static final String ANALYSIS_TASK_URL = "/api/mg/v2/analysis-configuration/tasks";

    @Value("${sz.unisinsight.client.id}")
    private String clientId;

    @Value("${sz.unisinsight.client.secret}")
    private String clientSecret;

    @Value("${sz.unisinsight.enable}")
    private boolean enable;

    public static final Map<String, ResultToken> map = new HashMap<>(1);

    @PostConstruct
    public void init() {
        if (enable){
            accessToken();
        }
    }

    private void accessToken() {
        try {
            ResponseEntity<ResultToken> forEntity = restTemplate.getForEntity(
                    ACCESS_TOKEN_URL + "?grant_type=client_credentials" +
                            "&client_id=" + clientId +
                            "&client_secret=" + clientSecret +
                            "&format=json"
                    , ResultToken.class);
            if (forEntity.getStatusCode() == HttpStatus.OK) {
                log.info("生成token成功");
                map.put("token", forEntity.getBody());
            }
        } catch (Exception e) {
            log.error("生成token失败{}", e.getMessage());
        }
    }


    /**
     * 刷新token 每小时执行一次
     */
//    @Scheduled(cron = "0 0 */1 * * ?")
//    @Scheduled(cron = "0 */1 * * * ?")
    private void refreshToken() {
        log.info("开始刷新token");
        ResultToken token = map.get("token");
        if (token == null) {
            accessToken();
        } else {
            try {
                ResponseEntity<ResultToken> forEntity = restTemplate.getForEntity(
                        ACCESS_TOKEN_URL + "?grant_type=refresh_token" +
                                "&client_id=" + clientId +
                                "&client_secret=" + clientSecret +
                                "&refresh_token=" + token.getRefresh_token() +
                                "&format=json"
                        , ResultToken.class);

                if (forEntity.getStatusCode() == HttpStatus.OK) {
                    log.info("刷新token成功");
                }
            } catch (Exception e) {
                log.error("刷新token失败{}", e.getMessage());
            }
        }
    }

    public Object getAnalysisTask() {
        HttpEntity<String> httpEntity = new HttpEntity<>(null, HttpUtil.getUnisinsightHttpHeaders(map.get("token").getAccess_token(), clientId));
        AnalysisDTO analysisDTO = AnalysisDTO.builder().source_type(1).page(1).size(100).build();
        Map<String, Object> objectMap = JSON.parseObject(JSON.toJSONString(analysisDTO), Map.class);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(ANALYSIS_TASK_URL);
        ResponseEntity<String> forEntity;
        if (!objectMap.isEmpty()) {
            for (Map.Entry<String, Object> e : objectMap.entrySet()) {
                //构建查询参数
                builder.queryParam(e.getKey(), e.getValue());
            }
            //拼接好参数后的URl//test.com/url?param1={param1}&param2={param2};
            String reallyUrl = builder.build().toString();
            forEntity = restTemplate.exchange(reallyUrl, HttpMethod.GET, httpEntity, String.class);
        } else {
            forEntity = restTemplate.exchange(ANALYSIS_TASK_URL, HttpMethod.GET, httpEntity, String.class);
        }
        return forEntity.getBody();
    }


    public Object addAnalysisTask() {
        AnalysisDTO analysisDTO = new AnalysisDTO();
        analysisDTO.setTask_name("测试");
        analysisDTO.setSource_type(1);
        Source source = new Source();
        source.setSource_id("113124314");
        source.setSource_name("测试source");
        source.setSub_type("1");
        source.setType("2");
        ArrayList<Source> sources = new ArrayList<>();
        sources.add(source);
        analysisDTO.setSources(sources);
        HttpEntity<AnalysisDTO> httpEntity = new HttpEntity<>(analysisDTO, HttpUtil.getUnisinsightHttpHeaders(map.get("token").getAccess_token(), clientId));
        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(ANALYSIS_TASK_URL
                , httpEntity, String.class);
        return stringResponseEntity.getBody();
    }

    public Object updateAnalysisTask() {
        String taskId = "a7b2097b60d24712ae202b68980816ab";
        AnalysisDTO analysisDTO = new AnalysisDTO();
        analysisDTO.setTask_name("测试22");
        analysisDTO.setSource_type(1);
        Source source = new Source();
        source.setSource_id("113124314");
        source.setSource_name("测试source");
        source.setSub_type("1");
        source.setType("2");
        ArrayList<Source> sources = new ArrayList<>();
        sources.add(source);
        analysisDTO.setSources(sources);
        HttpEntity<AnalysisDTO> httpEntity = new HttpEntity<>(analysisDTO, HttpUtil.getUnisinsightHttpHeaders(map.get("token").getAccess_token(), clientId));
        ResponseEntity<String> stringResponseEntity = restTemplate.exchange(ANALYSIS_TASK_URL + "/" + taskId
                , HttpMethod.PUT, httpEntity, String.class);
        return stringResponseEntity.getBody();
    }

    public Object deleteAnalysisTask() {
        String source_type = "1";
        ArrayList<String> ids = new ArrayList<>();
        ids.add("a7b2097b60d24712ae202b68980816ab");
        Map<String, Object> objectMap = new HashMap<>(2);
        objectMap.put("ids", ids);
        objectMap.put("source_type", source_type);
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(objectMap), HttpUtil.getUnisinsightHttpHeaders(map.get("token").getAccess_token(), clientId));
        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(ANALYSIS_TASK_URL + "/batch-delete"
                , httpEntity, String.class);
        return stringResponseEntity.getBody();
    }


    public Object lockStatusAnalysisTask() {
        String source_type = "1";
        Integer lock_state = 2;
        ArrayList<String> ids = new ArrayList<>();
        ids.add("441ef84c3dc44bfe97aee41988c27495");
        Map<String, Object> objectMap = new HashMap<>(2);
        objectMap.put("ids", ids);
        objectMap.put("source_type", source_type);
        objectMap.put("lock_state", lock_state);
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(objectMap), HttpUtil.getUnisinsightHttpHeaders(map.get("token").getAccess_token(), clientId));
        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(ANALYSIS_TASK_URL + "/lock-status"
                , httpEntity, String.class);
        return stringResponseEntity.getBody();
    }


    public Object statusAnalysisTask() {
        String source_type = "1";
        Integer status = 3;
        ArrayList<String> task_ids = new ArrayList<>();
        task_ids.add("441ef84c3dc44bfe97aee41988c27495");
        Map<String, Object> objectMap = new HashMap<>(2);
        objectMap.put("task_ids", task_ids);
        objectMap.put("source_type", source_type);
        objectMap.put("status", status);
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(objectMap), HttpUtil.getUnisinsightHttpHeaders(map.get("token").getAccess_token(), clientId));
        ResponseEntity<String> stringResponseEntity = restTemplate.exchange(ANALYSIS_TASK_URL + "/status"
                , HttpMethod.PUT, httpEntity, String.class);
        return stringResponseEntity.getBody();
    }

    public Object getAnalysisTaskDetailForTaskId() {
        String task_id = "441ef84c3dc44bfe97aee41988c27495";
        HttpEntity<String> httpEntity = new HttpEntity<>(null, HttpUtil.getUnisinsightHttpHeaders(map.get("token").getAccess_token(), clientId));
        ResponseEntity<String> forEntity = restTemplate.exchange(ANALYSIS_TASK_URL + "/" + task_id
                , HttpMethod.GET, httpEntity, String.class);
        return forEntity.getBody();
    }



    public Object getAnalysisTaskAnalysisDetailForTaskId() {
        String task_id = "441ef84c3dc44bfe97aee41988c27495";
        HttpEntity<String> httpEntity = new HttpEntity<>(null, HttpUtil.getUnisinsightHttpHeaders(map.get("token").getAccess_token(), clientId));
        ResponseEntity<String> forEntity = restTemplate.exchange(ANALYSIS_TASK_URL + "/" + task_id+"?page_num=1&page_size=100"
                , HttpMethod.GET, httpEntity, String.class);
        return forEntity.getBody();
    }
}
