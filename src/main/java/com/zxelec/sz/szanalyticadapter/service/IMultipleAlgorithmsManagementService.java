package com.zxelec.sz.szanalyticadapter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zxelec.sz.szanalyticadapter.domain.entity.MultipleAlgorithmsManagement;
import com.zxelec.sz.szanalyticadapter.domain.entity.Result;

public interface IMultipleAlgorithmsManagementService  extends IService<MultipleAlgorithmsManagement> {
    Result search(MultipleAlgorithmsManagement data);
}
