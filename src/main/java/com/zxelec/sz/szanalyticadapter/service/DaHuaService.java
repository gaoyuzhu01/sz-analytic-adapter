package com.zxelec.sz.szanalyticadapter.service;


import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.zxelec.sz.szanalyticadapter.domain.entity.Result;
import com.zxelec.sz.szanalyticadapter.domain.pojo.*;
import com.zxelec.sz.szanalyticadapter.task.DelayQueueManager;
import com.zxelec.sz.szanalyticadapter.task.DelayTask;
import com.zxelec.sz.szanalyticadapter.task.Prefix;
import com.zxelec.sz.szanalyticadapter.util.HttpUtil;
import com.zxelec.sz.szanalyticadapter.util.MD5Util;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class DaHuaService {

    @Resource(name = "daHuaRestTemplate")
    private RestTemplate restTemplate;


    @Value("${sz.dahua.username}")
    private String username;
    @Value("${sz.dahua.enable}")
    private boolean enable;

    @Value("${sz.dahua.passwd}")
    private String passwd;

    public static DelayQueueManager delayQueueManager;
    public static final Map<String, String> map = new HashMap<>(1);

    private static final String AUTHORIZE = "/videoService/accounts/authorize";
    private static final String KEEPALIVE = "/videoService/accounts/token/keepalive";
    private static final String CAPTURE = "/faceService/query/capture";
    private static final String REPOSITORY = "/faceService/retrieval/repository";
    private static final String NVNTASK = "/faceService/nvnTask";
    private static final String NVNTASK_RESULTLIST = "/faceService/nvnTask/resultList";
    private static final String FACEVERIFY = "/faceService/faceVerify";
    private static final String TASK = "/gsurvey/survey/task";
    private static final String TASKS_REVIEW = "/gsurvey/survey/tasks/review";
    private static final String TASKS_QUERY_REVIEW = "/gsurvey/survey/tasks/query/review";
    private static final String TASKS_QUERY = "/gsurvey/survey/tasks/query";
    private static final String TASKS_REVOKE = "/gsurvey/survey/tasks/revoke";
    private static final String QUERYPIC = "/vehicleService/rest/picQuery/batchImage";
    private static final String DETECT = "/vehicleService/rest/picQuery/detect";
    private static final String SURVEILLANCE = "/vehicleService/rest/surveillance";
    /**
     * 删除库成员
     */
    private static final String REMOVEMEMBERS = "/gsurvey/repository/members/remove";
    /**
     * 新增库成员
     */
    private static final String ADDMEMBER = "/gsurvey/repository/member";
    /**
     * 批量新增
     */
    private static final String ADDBATCHMEMBER = "/gsurvey/repository/member/batch";
    /**
     * 审核库成员
     */
    private static final String REVIEWMEMBER = "/gsurvey/repository/members/review";
    /**
     * 查询库成员审核列表
     */
    private static final String QUERYREVIEWMEMBER = "/gsurvey/repository/members/query/reviewmember";
    /**
     * 主题库详情
     */
    private static final String THEMEREPOSITORY = "/gsurvey/repository";
    /**
     * 检索主题库
     */
    private static final String QUERYTHEMEREPOSITORY = "/gsurvey/repository/query";


    private static final String RASDICBATCHQUERY = "/ras/dic/batch/query";

    public Result rasdicbatchquery(RasdicbatchqueryVO vo) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(vo), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.postForEntity(RASDICBATCHQUERY, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    @PostConstruct
    public void init() {
        if (enable) {
            authorize();
        }
    }

    private void authorize() {
        ResponseEntity<String> forEntity = null;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            Map<String, Object> objectMap = new HashMap<>(3);
            objectMap.put("userName", username);
            objectMap.put("clientType", "winpc");
            objectMap.put("ipAddress", InetAddress.getLocalHost().getHostAddress());
            HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(objectMap), headers);
            forEntity = restTemplate.postForEntity(AUTHORIZE, httpEntity, String.class);
            if (forEntity.getStatusCode() == HttpStatus.OK) {
                log.info("大华，生成token成功");
                map.put("token", forEntity.getBody());
            }
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                try {
                    AuthorizeUnauthorizedVO unauthorizedVO = JSON.parseObject(e.getResponseBodyAsString(), AuthorizeUnauthorizedVO.class);
                    String signature = null;
                    try {
                        if (StringUtils.isBlank(unauthorizedVO.getMethod())) {
                            signature = buildSignatureForEncryptType(unauthorizedVO);
                        } else if ("simple".equals(unauthorizedVO.getMethod())) {
                            signature = buildSignatureForSimple(unauthorizedVO);
                        }
                    } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
                        log.error("大华，生成Signature失败{}", e.getMessage());
                    }
                    Map<String, Object> objectMap = new HashMap<>();
                    objectMap.put("userName", username);
                    objectMap.put("signature", signature);
                    objectMap.put("randomKey", unauthorizedVO.getRandomKey());
                    objectMap.put("encryptType", unauthorizedVO.getEncryptType());
                    objectMap.put("clientType", "winpc");
                    objectMap.put("ipAddress", InetAddress.getLocalHost().getHostAddress());
                    HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(objectMap), headers);
                    forEntity = restTemplate.postForEntity(AUTHORIZE, httpEntity, String.class);
                    if (forEntity.getStatusCode() == HttpStatus.OK) {
                        log.info("大华，生成token成功");
                        AuthorizeOkVO authorizeOkVO = JSON.parseObject(forEntity.getBody(), AuthorizeOkVO.class);
                        map.put("token", authorizeOkVO.getToken());
                        delayQueueManager = new DelayQueueManager();
                        delayQueueManager.put(new DelayTask(Prefix.keepalive, authorizeOkVO.getUserId(), authorizeOkVO.getDuration() * 1000 * 3 / 4, () -> keepalive(authorizeOkVO)));
                    }
                } catch (UnknownHostException ex) {
                    log.error("大华，生成token失败{}", e.getMessage());
                }
            }
        } catch (UnknownHostException e) {
            log.error("大华，生成token失败{}", e.getMessage());
        }
    }

    private void keepalive(AuthorizeOkVO authorizeOkVO) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            String token = map.get("token");
            headers.add("X-Subject-Token", token);
            Map<String, Object> objectMap = new HashMap<>(1);
            objectMap.put("token", token);
            HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(objectMap), headers);
            ResponseEntity<String> responseEntity = restTemplate.exchange(KEEPALIVE, HttpMethod.PUT, httpEntity, String.class);
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                log.debug("大华，token 保活成功");
            }
            delayQueueManager.put(new DelayTask(Prefix.keepalive, authorizeOkVO.getUserId(), authorizeOkVO.getDuration() * 1000 * 3 / 4, () -> keepalive(authorizeOkVO)));

        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                authorize();
            }
        }
    }

    private String buildSignatureForSimple(AuthorizeUnauthorizedVO unauthorizedVO) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String encrypted = MD5Util.md5(username + ":" + unauthorizedVO.getRealm() + ":" + passwd);
        return MD5Util.md5(encrypted + ":" + unauthorizedVO.getRandomKey());
    }

    private String buildSignatureForEncryptType(AuthorizeUnauthorizedVO unauthorizedVO) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        if ("MD5".equals(unauthorizedVO.getEncryptType())) {
            String value0 = MD5Util.md5(passwd);
            String value1 = MD5Util.md5(username + value0);
            String tmp = MD5Util.md5(value1);
            String encrypted = MD5Util.md5(username + ":" + unauthorizedVO.getRealm() + ":" + tmp);
            return MD5Util.md5(encrypted + ":" + unauthorizedVO.getRandomKey());
        }
        return null;
    }


    public Result picQuery(JSONObject json) {

        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(json), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.postForEntity(QUERYPIC+"?page=1&pageSize=1000", httpEntity, String.class);
//        +"?page=1&pageSize=10"
        return Result.success(entity.getBody());
    }

    public Result capture(CaptureVO vo) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(vo), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        String url = CAPTURE;
        if (vo.getStart() != null && vo.getLimit() != null) {
            url = CAPTURE + "?start=" + vo.getStart() + "&limit=" + vo.getLimit();
        }
        ResponseEntity<String> entity = restTemplate.postForEntity(url, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result surveillance(SurveillanceVO vo) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(vo), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.postForEntity(SURVEILLANCE, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result faceVerify(FaceVerifyVO vo) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(vo), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.postForEntity(FACEVERIFY, httpEntity, String.class);
        return Result.success(entity.getBody());
    }


    public Result nvnTask(NvnTaskVo vo) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(vo), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.postForEntity(NVNTASK, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result nvnTaskResultList(NvnTaskResultListVo vo) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(vo), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.postForEntity(NVNTASK_RESULTLIST, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result repository(RepositoryVO vo) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(vo), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        String url = REPOSITORY;
        if (vo.getResultPictureType() != null) {
            url = REPOSITORY + "?resultPictureType=" + vo.getResultPictureType();
        }
        ResponseEntity<String> entity = restTemplate.postForEntity(url, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result task(TaskVO vo) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(vo), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.postForEntity(TASK, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result tasksReview(TasksReviewVO vo) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(vo), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.exchange(TASKS_REVIEW, HttpMethod.PUT, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result tasksQuery(TasksQueryVO vo) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(vo), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.postForEntity(TASKS_QUERY, httpEntity, String.class);
        return Result.success(entity.getBody());
    }


    public Result tasksQueryReview(TasksQueryReviewVO vo) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(vo), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.postForEntity(TASKS_QUERY_REVIEW, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result taskUpdate(TaskVO vo) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(vo), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.exchange(TASK + "/" + vo.getSurveyId(), HttpMethod.PUT, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result taskRevoke(TaskRevokeVO vo) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(vo), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.exchange(TASKS_REVOKE, HttpMethod.PUT, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result detect() {
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("uri", "http://10.50.0.185:9000/cone-file-dev/0c36011923d7d908880d85c4a8f57798.jpg");
        try {
            File file = new File("/Users/yuexiangyu/Downloads/0c36011923d7d908880d85c4a8f57798.jpg");
            byte[] bytes = FileUtils.readFileToByteArray(file);
            objectMap.put("data", new String(Base64.getEncoder().encode(bytes), StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        objectMap.put("type", "image/jpg");
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(objectMap), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.postForEntity(DETECT, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result removeMembers(RemoverMembersDTO t) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(t), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.exchange(REMOVEMEMBERS, HttpMethod.PUT, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result addMember(MemberDTO t) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(t), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.postForEntity(ADDMEMBER, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result addBatchMember(BatchMemberDTO t) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(t), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.postForEntity(ADDBATCHMEMBER, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result reviewMember(ReviewMemberDTO t) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(t), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.exchange(REVIEWMEMBER, HttpMethod.PUT, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result queryReviewMember(QueryReviewMemberDTO t) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(t), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> entity = restTemplate.postForEntity(QUERYREVIEWMEMBER, httpEntity, String.class);
        return Result.success(entity.getBody());
    }

    public Result themeRepository(String repositoryId) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(repositoryId), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> forEntity = restTemplate.exchange(THEMEREPOSITORY + "/" + repositoryId, HttpMethod.GET, httpEntity, String.class);
        return Result.success(forEntity.getBody());
    }

    public Result deletethemeRepository(String repositoryId) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(repositoryId), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> forEntity = restTemplate.exchange(THEMEREPOSITORY + "/" + repositoryId, HttpMethod.DELETE, httpEntity, String.class);
        return Result.success(forEntity.getBody());
    }

    public Result editThemeRepository(String repositoryId, EditThemeVO t) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(t), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> forEntity = restTemplate.exchange(THEMEREPOSITORY + "/" + repositoryId, HttpMethod.PUT, httpEntity, String.class);
        return Result.success(forEntity.getBody());
    }

    /**
     * 4.检索主题库
     *
     * @return
     */
    public Result queryThemeRepository(QueryThemeDTO t) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(t), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> forEntity = restTemplate.postForEntity(QUERYTHEMEREPOSITORY, httpEntity, String.class);
        return Result.success(forEntity.getBody());
    }

    public Result addThemeRepository(AddThemeDTO t) {
        HttpEntity<String> httpEntity = new HttpEntity<>(JSON.toJSONString(t), HttpUtil.getDaHuaHttpHeaders(map.get("token")));
        ResponseEntity<String> forEntity = restTemplate.exchange(THEMEREPOSITORY, HttpMethod.POST, httpEntity, String.class);
        return Result.success(forEntity.getBody());
    }
}
