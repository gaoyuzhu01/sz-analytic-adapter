package com.zxelec.sz.szanalyticadapter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxelec.sz.szanalyticadapter.constant.HttpStatus;
import com.zxelec.sz.szanalyticadapter.domain.entity.Attachments;
import com.zxelec.sz.szanalyticadapter.domain.entity.Result;
import com.zxelec.sz.szanalyticadapter.mapper.AttachmentsMapper;
import com.zxelec.sz.szanalyticadapter.service.IAttachmentsService;
import io.minio.ObjectWriteResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zxelec.sz.szanalyticadapter.service.MinioService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.List;

@Service
@Slf4j
public class AttachmentsServiceImpl extends ServiceImpl<AttachmentsMapper, Attachments> implements IAttachmentsService {


    @Autowired
    private MinioService minioService;


    @Override
    public Result upload(InputStream inputStream, String originalFilename, String contentType, String mamId) {
        String minioPath = mamId + "/" + originalFilename;
        ObjectWriteResponse objectWriteResponse = minioService.putObject(inputStream, minioPath, contentType);
        String idStr = IdWorker.getIdStr();
        if (objectWriteResponse != null) {
            try {
                String minioUrl = minioService.presignedGetObject(minioPath);
                Attachments attachments = Attachments.builder()
                        .name(originalFilename)
                        .id(idStr)
                        .mamId(mamId)
                        .minioPath(minioPath)
                        .minioUrl(minioUrl).build();

                boolean save = save(attachments);
                if (!save) {
                    minioService.removeObject(minioPath);
                    return Result.error("上传失败");
                }
                return Result.success("上传成功");
            } catch (Exception e) {
                log.error(e.getMessage());
                removeById(idStr);
                minioService.removeObject(minioPath);
            }
        }
        return Result.error("上传失败");
    }


    @Override
    public Result delete(Attachments attachments) {

        boolean b = removeById(attachments.getId());
        if (b) {
            minioService.removeObject(attachments.getMinioPath());
        }
        return Result.success();
    }

    @Override
    public Result list(String mamId) {
        LambdaQueryWrapper<Attachments> objectLambdaQueryWrapper = new LambdaQueryWrapper<>();
        objectLambdaQueryWrapper.eq(Attachments::getMamId, mamId);
        List<Attachments> list = list(objectLambdaQueryWrapper);
        return Result.success(list);
    }
}
