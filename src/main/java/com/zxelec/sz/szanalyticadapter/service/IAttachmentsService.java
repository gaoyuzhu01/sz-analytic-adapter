package com.zxelec.sz.szanalyticadapter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zxelec.sz.szanalyticadapter.domain.entity.Attachments;
import com.zxelec.sz.szanalyticadapter.domain.entity.Result;

import java.io.InputStream;

public interface IAttachmentsService extends IService<Attachments> {
    Result upload(InputStream inputStream, String originalFilename, String contentType, String mamId);

    Result delete(Attachments attachments);

    Result list(String mamId);
}
