package com.zxelec.sz.szanalyticadapter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zxelec.sz.szanalyticadapter.domain.entity.MultipleAlgorithmsManagement;
import com.zxelec.sz.szanalyticadapter.domain.entity.PageResult;
import com.zxelec.sz.szanalyticadapter.domain.entity.Result;
import com.zxelec.sz.szanalyticadapter.mapper.MultipleAlgorithmsManagementMapper;
import com.zxelec.sz.szanalyticadapter.service.IMultipleAlgorithmsManagementService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class MultipleAlgorithmsManagementServiceImpl extends ServiceImpl<MultipleAlgorithmsManagementMapper, MultipleAlgorithmsManagement> implements IMultipleAlgorithmsManagementService {
    @Override
    public Result search(MultipleAlgorithmsManagement data) {
        LambdaQueryWrapper<MultipleAlgorithmsManagement> queryWrapper = new LambdaQueryWrapper<>();
        if (null != data.getAlgorithmNameCode()) {
            queryWrapper.eq(MultipleAlgorithmsManagement::getAlgorithmNameCode, data.getAlgorithmNameCode());
        }
        if (null != data.getAlgorithmTypeCode()) {
            queryWrapper.eq(MultipleAlgorithmsManagement::getAlgorithmTypeCode, data.getAlgorithmTypeCode());
        }
        if (null != data.getManufacturerNameCode()) {
            queryWrapper.eq(MultipleAlgorithmsManagement::getManufacturerNameCode, data.getManufacturerNameCode());
        }
        if (StringUtils.isNotBlank(data.getTitle())) {
            queryWrapper.like(MultipleAlgorithmsManagement::getTitle, data.getTitle());
        }
        Page<MultipleAlgorithmsManagement> page = new Page<>(data.getPage().getPageNum(), data.getPage().getPageSize());
        Page<MultipleAlgorithmsManagement> list = page(page, queryWrapper);
        return Result.success(new PageResult(list));
    }
}
