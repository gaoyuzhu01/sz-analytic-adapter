package com.zxelec.sz.szanalyticadapter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxelec.sz.szanalyticadapter.domain.entity.Attachments;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AttachmentsMapper extends BaseMapper<Attachments> {
}
