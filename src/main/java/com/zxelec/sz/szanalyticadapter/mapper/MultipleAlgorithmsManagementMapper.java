package com.zxelec.sz.szanalyticadapter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxelec.sz.szanalyticadapter.domain.entity.MultipleAlgorithmsManagement;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MultipleAlgorithmsManagementMapper extends BaseMapper<MultipleAlgorithmsManagement> {
}
