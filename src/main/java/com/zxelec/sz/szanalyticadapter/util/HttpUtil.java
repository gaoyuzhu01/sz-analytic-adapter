package com.zxelec.sz.szanalyticadapter.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * 服务工具类
 *
 * @author kzf
 * @date 2023/7/19 17:02
 */
public class HttpUtil {


    /**
     * 获取调用视综服务的请求头
     *
     * @return
     */
    public static HttpHeaders getUnisinsightHttpHeaders(String token, String clientId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("Authorization", token);
        headers.add("User", "usercode:" + clientId);
        headers.add("Cookie", "usercode=" + clientId);
        return headers;
    }


    public static HttpHeaders getDaHuaHttpHeaders(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("X-Subject-Token", token);
        headers.add("X-Api-Version", "V1.0");
        return headers;
    }
}
