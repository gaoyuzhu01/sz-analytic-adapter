package com.zxelec.sz.szanalyticadapter.config;

import com.zxelec.sz.szanalyticadapter.http.HttpRestTemplateBuilder;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * 调用视综服务的restTeimplate配置
 *
 * @author kzf
 * @date 2023/7/18 16:33
 */
@Configuration
public class RestTemplateConfig {

    //始终服务的url
    @Value("${sz.unisinsight.url}")
    private String unisinsightUrl;


    @Value("${sz.dahua.url}")
    private String daHuaUrl;

    @Value("${sz.dahua1400.url}")
    private String daHua1400Url;

    @Bean("unisinsightRestTemplate")
    public RestTemplate unisinsightRestTemplate(){
        HttpRestTemplateBuilder buildRest = HttpRestTemplateBuilder.build();
        return buildRest.buildDefaultRestTemplate(unisinsightUrl);
    }


    @Bean("daHuaRestTemplate")
    public RestTemplate daHuaRestTemplate(){
        HttpRestTemplateBuilder buildRest = HttpRestTemplateBuilder.build();
        return buildRest.buildDefaultRestTemplate(daHuaUrl);
    }

    @Bean("daHua14000RestTemplate")
    public RestTemplate daHua1400RestTemplate(){
        HttpRestTemplateBuilder buildRest = HttpRestTemplateBuilder.build();
        RestTemplate restTemplate = buildRest.buildDefaultRestTemplate(daHua1400Url);
//        HttpClient httpclient = HttpClientBuilder.create().build();
//        RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory(httpclient));
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                if (response.getRawStatusCode() != 401) {
                    super.handleError(response);
                }
            }
        });

        restTemplate.getMessageConverters().set(1, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        return restTemplate;
    }
}
