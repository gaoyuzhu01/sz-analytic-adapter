package com.zxelec.sz.szanalyticadapter.http;

import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.*;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 构建模拟http请求对象
 */
@Slf4j
public class HttpRestTemplateBuilder {


    private HttpRestTemplateBuilder(){

    }

    public static HttpRestTemplateBuilder build(){
        return  new HttpRestTemplateBuilder();
    }

    /**
     * 构建基于默认的无认证的RestTemplate
     * @return
     */
    public RestTemplate buildDefaultRestTemplate(String url){
        HttpClientBuilder httpClientBuilder = httpClientBuilder();
        return getRestTemplate(url,clientHttpRequestFactory(httpClientBuilder));
    }



    //统一返回restTempate对象,解决前缀的拼接
    private  RestTemplate getRestTemplate(String basicHttpUrl, ClientHttpRequestFactory requestFactory){
        RestTemplate restTemplate=new RestTemplate(requestFactory){

            @Override
            public <T> T execute(URI url, HttpMethod method, RequestCallback requestCallback, ResponseExtractor<T> responseExtractor) throws RestClientException {
                String s = url.toString();
                url=URI.create(basicHttpUrl+s);
                log.debug("请求接口地址={}",url);
                T resulEntity= super.execute(url, method, requestCallback, responseExtractor);
                log.debug("响应结果={}", JSON.toJSONString(resulEntity));
                return resulEntity;
            }


            @Override
            public <T> T execute(String url, HttpMethod method, RequestCallback requestCallback, ResponseExtractor<T> responseExtractor, Map<String, ?> uriVariables) throws RestClientException {
                url=basicHttpUrl+url;
                log.debug("请求接口地址={}",url);
                T resulEntity=  super.execute(url, method, requestCallback, responseExtractor, uriVariables);
                log.debug("响应结果={}", JSON.toJSONString(resulEntity));
                return resulEntity;
            }

            @Override
            public <T> T execute(String url, HttpMethod method, RequestCallback requestCallback, ResponseExtractor<T> responseExtractor, Object... uriVariables) throws RestClientException {
                url=basicHttpUrl+url;
                log.debug("请求接口地址={}",url);
                T resulEntity=   super.execute(url, method, requestCallback, responseExtractor, uriVariables);
                log.debug("响应结果={}", JSON.toJSONString(resulEntity));
                return resulEntity;
            }

        };
        return restTemplate;
    }



    /**
     * 构建基于Basic认证的RestTemplate
     * @param username
     * @param password
     * @return
     */
    public RestTemplate buildBasicRestTemplate(String url, String username, String password, ClientHttpRequestInterceptor...interceptors){
        HttpClientBuilder httpClientBuilder = httpClientBuilder();
        // 新增Basic认证拦截器
        List<ClientHttpRequestInterceptor> interceptorList = new ArrayList<>();
        if(interceptors!=null&&interceptors.length>0) {
        	for(int i=0;i<interceptors.length;i++) {
        		interceptorList.add(interceptors[i]);
        	}
        }
        interceptorList.add(new BasicAuthenticationInterceptor(username, password));
        // 创建RestTemplate
        RestTemplate basicRestTemplate = getRestTemplate(url,clientHttpRequestFactory(httpClientBuilder));
        basicRestTemplate.setInterceptors(interceptorList);
        return basicRestTemplate;
    }

    /**
     * 构建基于Digest认证的RestTemplate
     * @param username
     * @param password
     * @return
     */
    public RestTemplate buildDigestRestTemplate(String url,String username, String password,ClientHttpRequestInterceptor ...interceptors){
        HttpClientBuilder httpClientBuilder = httpClientBuilder();

        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials =
                new UsernamePasswordCredentials(username, password);
        provider.setCredentials(AuthScope.ANY, credentials);

        httpClientBuilder.setDefaultCredentialsProvider(provider);
        List<ClientHttpRequestInterceptor> interceptorsList = new ArrayList<>();
        if(interceptors!=null&&interceptors.length>0) {
        	for(int i=0;i<interceptors.length;i++) {
        		interceptorsList.add(interceptors[i]);
        	}
        }
        // 创建RestTemplate
        RestTemplate basicRestTemplate = getRestTemplate(url,clientHttpRequestFactory(httpClientBuilder));
        basicRestTemplate.setInterceptors(interceptorsList);
        return basicRestTemplate;
    }

    /**
     * 新增auth2方式认证
     * @return
     */
    public RestTemplate buildAuth2RestTemplare(String url,final String username,final  String password,Auth2Handler loginHandler) {
        HttpClientBuilder httpClientBuilder = httpClientBuilder();
        // 新增sessionId认证拦截器
        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new ClientHttpRequestInterceptor(){

            @Override
            public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes, ClientHttpRequestExecution clientHttpRequestExecution) throws IOException {
                HttpHeaders headers = httpRequest.getHeaders();
                if(loginHandler!=null){
                    String bearer = loginHandler.handlerToken(username, password);
                    headers.set("Authorization","Bearer "+bearer);
                }
                 ClientHttpResponse response=null;
            	 response=clientHttpRequestExecution.execute(httpRequest,bytes);
            	 if(response.getRawStatusCode()==HttpStatus.UNAUTHORIZED.value()) {
            		 //清除临时token
                     log.debug("服务端返回401，token失效，重新获取token");
            		 loginHandler.clearInvalidToken();
            		 if(loginHandler!=null) {
            			 response=this.intercept(httpRequest, bytes, clientHttpRequestExecution);
            		 }
            	 }
              
                return response;
            }
        });
        // 创建RestTemplate
        RestTemplate basicRestTemplate = getRestTemplate(url,clientHttpRequestFactory(httpClientBuilder));
        basicRestTemplate.setInterceptors(interceptors);
        return basicRestTemplate;
    }


    /**
     * HttpClientBuilder对象
     * @return
     */
    public static HttpClientBuilder httpClientBuilder(){
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        httpClientBuilder.setSSLContext(sslContext());
        httpClientBuilder.setConnectionManager(poolingHttpClientConnectionManager());
        httpClientBuilder.setRetryHandler(new DefaultHttpRequestRetryHandler(3, true)); // 重试次数
        return httpClientBuilder;
    }


    /**
     * HttpComponentsClient工厂对象
     * @param httpClientBuilder
     * @return
     */
    public HttpComponentsClientHttpRequestFactory clientHttpRequestFactory(HttpClientBuilder httpClientBuilder){
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory =
                new HttpComponentsClientHttpRequestFactory(httpClientBuilder.build()); // httpClient连接配置
        clientHttpRequestFactory.setConnectTimeout(1000*60); // 连接超时, 默认20000毫秒
        clientHttpRequestFactory.setReadTimeout(1000*60*3);      // 数据读取超时时间, 默认30000毫秒
        clientHttpRequestFactory.setConnectionRequestTimeout(1000*60); // 连接不够用的等待时间, 默认20000毫秒

        //设置HTTP进程执行状态工厂类
        clientHttpRequestFactory.setHttpContextFactory(new HttpContextFactory());

        return clientHttpRequestFactory;
    }


    public static PoolingHttpClientConnectionManager poolingHttpClientConnectionManager(){
            try {
                HostnameVerifier hostnameVerifier = NoopHostnameVerifier.INSTANCE;
                SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext(), hostnameVerifier);
                Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                        .register("http", PlainConnectionSocketFactory.getSocketFactory())
                        .register("https", sslConnectionSocketFactory).build();// 注册http和https请求
                // 开始设置连接池
                PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
                poolingHttpClientConnectionManager.setMaxTotal(
                        500); // 最大连接数500
                poolingHttpClientConnectionManager.setDefaultMaxPerRoute(
                        1000); // 同路由并发数100
                return poolingHttpClientConnectionManager;
            }catch (Exception e){
                return null;
            }
    }

    public static SSLContext sslContext(){
        SSLContext sslContext = null;
        try {
            sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
                public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                    return true;
                }
            }).build();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
        return sslContext;
    }


    /**
     * auth2d登录
     */
    public static abstract class  Auth2Handler{
    	
    	public static String tokenId="";
    	public static long endDate=0;
    	private static ReentrantLock lock=new ReentrantLock();
    	
    	public void setToken(String tokenId,long endDate) {
    		Auth2Handler.tokenId=tokenId;
    		Auth2Handler.endDate=endDate;
    	}
    	
        /**
         * 获取token
         * @param username
         * @param password
         * @return
         */
        public abstract String handlerToken(String username,String password);
        
        public void clearInvalidToken() {
        	try {
        		lock.lock();
        		tokenId="";
            	endDate=0;
        	}finally {
        		lock.unlock();
        	}
        }
    }
}
