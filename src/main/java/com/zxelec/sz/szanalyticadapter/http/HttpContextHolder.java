package com.zxelec.sz.szanalyticadapter.http;

import org.apache.http.client.config.RequestConfig;
import org.springframework.core.NamedThreadLocal;

/**
 * @Description RequestConfig配置
 * @Author zhuxiaoyue
 * @Date 2023/4/19 16:21
 */
public class HttpContextHolder {

    private static final ThreadLocal<RequestConfig> threadLocal = new NamedThreadLocal<>(("HTTP进程执行状态上下文"));

    public static void bind(RequestConfig requestConfig) {
        threadLocal.set(requestConfig);
    }

    public static RequestConfig peek() {
        return threadLocal.get();
    }

    public static void unbind() {
        threadLocal.remove();
    }
}
