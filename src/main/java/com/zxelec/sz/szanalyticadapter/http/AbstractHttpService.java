package com.zxelec.sz.szanalyticadapter.http;


import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

/**
 * http模拟请求父类
 */

public abstract class AbstractHttpService {

     private RestTemplate restTemplate;

     protected RestTemplate getRestTemplate(){
        return   this.restTemplate;
     }

     protected abstract String getAuthtype();
     protected abstract String getUsername();
     protected abstract String getPassword();
     protected abstract String getUrl();


    //获取token
     protected HttpRestTemplateBuilder.Auth2Handler getAuth2Handler(){
         return null;
     }
     
      @PostConstruct
      public void init(){
         //认证类型
         String authtype = getAuthtype();
         String username=getUsername();
         String password = getPassword();
         String url=getUrl();
          //构建模拟请求对象
         HttpRestTemplateBuilder buildRest = HttpRestTemplateBuilder.build();
          if("basic".equals(authtype)){
             this.restTemplate=buildRest.buildBasicRestTemplate(url,username,password);
         }else if("digest".equals(authtype)){
              this.restTemplate=buildRest.buildDigestRestTemplate(url,username,password);
          }else if ("auth2".equals(authtype)){
              this.restTemplate=buildRest.buildAuth2RestTemplare(url,username,password,getAuth2Handler());
          }else{
              this.restTemplate=buildRest.buildDefaultRestTemplate(url);
          }
      }



}
