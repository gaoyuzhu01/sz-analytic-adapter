package com.zxelec.sz.szanalyticadapter.enums;

import lombok.Getter;

@Getter
public enum AlgorithmNameEnum {

    FACE_ALIGNMENT_ALGORITHMS(1,"人脸比对算法"),
    FACE_RECOGNITION_ANALYSIS_ALGORITHM(2,"人脸识别分析算法"),
    TARGET_IDENTIFICATION_ANALYSIS_ALGORITHM(3,"目标识别分析算法"),
    STRUCTURAL_ANALYSIS_ALGORITHM(4,"结构化分析算法"),
    OBJECT_DETECTION(5,"目标检测"),
    NON_MOTOR_VEHICLE_COMPARISON_ALGORITHM(6,"非机动车比对算法"),
    FACE_RECOGNITION(7,"人脸识别"),
    STRUCTURING(8,"结构化"),
    VEHICLE_COMPARISON_ALGORITHM(9,"机动车比对算法"),
    PERSONNEL_THAN_ALGORITHM(10,"人员比对算法"),
    OBJECT_RECOGNITION(11,"目标识别");


    AlgorithmNameEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    //状态码
    private Integer code;
    //内容
    private String msg;

}
