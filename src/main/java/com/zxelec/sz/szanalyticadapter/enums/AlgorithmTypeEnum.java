package com.zxelec.sz.szanalyticadapter.enums;

import lombok.Getter;

@Getter
public enum AlgorithmTypeEnum {

    ANALYSIS_ALGORITHM(1,"分析算法"),
    ALIGNMENT_ALGORITHM(2,"比对算法"),
    COMBINATORIAL_ALGORITHM(3,"组合算法");

    AlgorithmTypeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    //状态码
    private Integer code;
    //内容
    private String msg;
}
