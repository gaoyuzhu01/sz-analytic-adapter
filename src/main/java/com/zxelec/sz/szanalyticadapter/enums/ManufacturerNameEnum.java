package com.zxelec.sz.szanalyticadapter.enums;

import lombok.Getter;

@Getter
public enum ManufacturerNameEnum {

    UNISINSIGHT(1,"华智"),
    HAIKANG(2,"海康"),
    DAHUA(3,"大华");

    ManufacturerNameEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    //状态码
    private Integer code;
    //内容
    private String msg;
}
